Source: pappl
Maintainer: Debian Printing Team <debian-printing@lists.debian.org>
Uploaders: Thorsten Alteholz <debian@alteholz.de>
Section: net
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 libavahi-client-dev,
 libcups2-dev,
 libcupsimage2-dev,
 libgnutls28-dev,
 libjpeg-dev,
 libpam0g-dev,
 libpng-dev,
 libusb-1.0-0-dev,
 zlib1g-dev
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/printing-team/pappl
Vcs-Git: https://salsa.debian.org/printing-team/pappl.git
Homepage: https://www.msweet.org/pappl/
Rules-Requires-Root: no

Package: libpappl1
Architecture: any
Multi-Arch: same
Section: libs
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: C-based framework/library for developing CUPS Printer Applications
 PAPPL is a simple C-based framework/library for developing CUPS Printer
 Applications, which are the recommended replacement for printer drivers. It
 was specifically developed to support LPrint and a future Gutenprint Printer
 Application but is sufficiently general purpose to support any kind of printer
 or driver that can be used on desktops, servers, and in embedded environments.

Package: libpappl-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends:
 libcups2-dev,
 libgnutls28-dev,
 libjpeg-dev,
 libpam0g-dev,
 libpappl1 (= ${binary:Version}),
 libpng-dev,
 libusb-1.0-0-dev,
 zlib1g-dev,
 libavahi-client-dev,
 ${misc:Depends},
Description: C-based framework/library for developing CUPS Printer Applications - Headers
 PAPPL is a simple C-based framework/library for developing CUPS Printer
 Applications, which are the recommended replacement for printer drivers. It
 was specifically developed to support LPrint and a future Gutenprint Printer
 Application but is sufficiently general purpose to support any kind of printer
 or driver that can be used on desktops, servers, and in embedded environments.
 .
 This package contains the static library, headers and documentation.
